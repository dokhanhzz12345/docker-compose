package com.example.crudhibernatespring.service;

import com.example.crudhibernatespring.bean.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {
    public Book addBook(Book book);

    public List<Book> addBooks(List<Book> books);

    public List<Book> getBooks();

    public Book getBookById(int id);

    public String deleteBook(int id);

    public Book updateBook(Book book);
}
